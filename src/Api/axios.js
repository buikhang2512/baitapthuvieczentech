import axios from "axios";

const instance = axios.create({
    baseURL: "https://5f6c438a8d5ef70016ca0918.mockapi.io/api/v1/",
});

export default instance;