import { result, values } from 'lodash';
import memoize from'memoize-one';

const IntFormat = {
    intl: {},
    setIntl(intl) {
        this.intl = {...intl};
    },

    text(objectValue) {
        return messageIntl(objectValue.id, objectValue.defaultMessage, objectValue.values)
    },
    label(objectValue, isPlaceholder = false) {
        if(isPlaceholder) {
            return {
                isPlaceholder: objectValue.id,
                defaultplaceholder: objectValue.defaultMessage
            }
        } else {
            return {
                label: objectValue.id,
                defaultlabel: objectValue.defaultMessage
            }
        }
    },

    default(objectValue) {
        return {
            id: objectValue.id,
            defaultMessage: objectValue.defaultMessage 
        }
    },

    setMessageLanguage(objMessList, keyObj = "defaultMessage") {
        let result = {}
        Object.keys(objMessList).map(objValue => {
           result[objMessList[objValue].id] = objMessList[objValue][keyObj]
        })
        return result
     },
}

const messageIntl = memoize((id,defaultText,values) => {
    return IntFormat.intl.formatMessage ?
        IntFormat.intl.formatMessage({
            id: id,
            defaultMessage: defaultText || id
        }, values ? {...values} : undefined)

        : (defaultText || id)
})

const LanguageFormat = {
    vi: "vi",
    en: "en",
    fr: "fr"
}

export {IntFormat,LanguageFormat}