import { ModuleAR_VI,ModuleAR_EN } from "."

const vi_VN = {ModuleAR_VI}

const en_US = {ModuleAR_EN}

export const MessageLanguage = {
    vi_VN: {
        key:"vi-VN",
        locale:"vi-VN",
        defaultLocale:"vi-VN",
        message: vi_VN
    },
    en_US: {
        key: "en-US",
        locale:"en-US",
        defaultLocale:"en-US",
        message: en_US
    }
}