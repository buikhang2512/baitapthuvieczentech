import { IntFormat, LanguageFormat } from "../intlFormat";
import { Mess_Component, Mess_ArDmKh } from "./variable";


const ModuleAR_VI = {
    ...IntFormat.setMessageLanguage(Mess_Component),
    ...IntFormat.setMessageLanguage(Mess_ArDmKh)
}

const ModuleAR_EN = {
    ...IntFormat.setMessageLanguage(Mess_Component, LanguageFormat.en),
    ...IntFormat.setMessageLanguage(Mess_ArDmKh, LanguageFormat.en)
}
console.log(IntFormat.setMessageLanguage(Mess_ArDmKh, LanguageFormat.en))

export { ModuleAR_EN, ModuleAR_VI}