const prefixComponent = "component.";
const prefixArDmKh = "ardmkh."

//id và defaultmesage là tên props khi sử dụng react-intl

export const Mess_Component = {
    CreateNew: {
        id: prefixArDmKh + "CreateNew",
        defaultMessage:"Thêm mới", // mặc định là tiếng việt 
        en: "Create new",          // Tiếng anh
        fr: "",                    // Tiếng pháp
    },
    Edit: {
        id: prefixArDmKh + "Edit",
        defaultMessage:"Sửa", // mặc định là tiếng việt
        en: "Edit",          // Tiếng anh
        fr: "",                    // Tiếng pháp
    }
}

export const Mess_ArDmKh = {
    Ma: {
        id: prefixArDmKh + "ma_kh",
        defaultMessage: "Mã khách hàng",
        en: ""
    }
}