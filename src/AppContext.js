import React, { createContext,useEffect,useState } from 'react';
import { IntlProvider, createIntl, createIntlCache } from 'react-intl';
import { IntFormat } from './utils/intlFormat';
import { MessageLanguage } from './utils/zLanguage/Messages';


// const cache = createIntlCache()
// const AppContext = createContext();

// class AppProviderWrapper extends React.Component {
//    constructor(props) {
//       super();
//     //   this.hubConnection = null;
//     //   this.num_of_tryconnect = 0;
//     //   this.initSignalRConnection(false)
//       this.setInit(MessageLanguage.en_US);

//       this.state = {
//          language: MessageLanguage.en_US,
//       };
//    }

//    setInit(language) {
//       //Global intl
//       var intl = createIntl(language, cache);
//     //   ZenHelper.SetIntl(intl);
//     //   IntlFormat.setIntl(intl)
//    }

//    onChangeLanguage = (locale = "vi-VN") => {
//       let language = { ...this.state.language }
//       if (locale === "en-US") {
//          language = { ...language, ...MessageLanguage.en_US }
//       }

//       this.setInit(language);
//       this.setState({ language: language });
//    }

//    // **************************************** HUBS ****************************************\\
// //    initSignalRConnection = (isLogin = true) => {
// //       if (auth.getToken() !== null) {
// //          this.hubConnection = new signalR.HubConnectionBuilder()
// //             .withUrl(ZenApp.baseUrl + `zenbook-hubs?tenantid=${auth.getTenantId()}`)
// //             .build()

// //          this.listenerChangedNotify()
// //          this.startSignalR(isLogin)
// //       }
// //    }

// //    startSignalR = (startFromLogin) => {
// //       this.num_of_tryconnect++;
// //       this.hubConnection.start()
// //          .then((con) => {
// //             console.log("connected")
// //             cacheData({ tenant: auth.getTenantId() }, startFromLogin)
// //          })
// //          .catch((err) => {
// //             console.log('Error while stating connection signalR: ', err)
// //          })
// //    }

// //    disconnectSignalR = () => {
// //       this.hubConnection.stop()
// //          .then(() => {
// //             console.log("disconnected")
// //          })
// //          .catch((err) => {
// //             console.log('Error while disconnect signalR: ', err)
// //          })
// //    }

// //    listenerChangedNotify = () => {
// //       this.hubConnection.on("InvalidateCache", (notify) => {
// //          console.log("ChangedNotifyListener", notify)
// //          cacheData(notify)
// //       })
// //    }

//    render() {
//       const { children } = this.props;
//       const { language } = this.state;

//       return (
//          <GlobalStore language={language}
//             changeLaguage={this.onChangeLanguage}
//          >
//                <IntlProvider {...language}>
//                   {children}
//                </IntlProvider>
//          </GlobalStore>
//       );
//    }
// }

// const GlobalStore = (props) => {
//    const { language, changeLaguage} = props

//    const [store, setStore] = useState({
//       // todos: ["Make the bed", "Take out the trash"],
//       messLanguage: language,
//       menus: []
//    });
//    const [actions, setActions] = useState({
//       // addTask: (title) => setStore({ ...store, todos: store.todos.concat(title) }),
//       changeLaguage: changeLaguage,
//       setMenu: (menus) => setStore({ ...store, menus: menus })
//    });

//    useEffect(() => {
//       setStore({ ...store, messLanguage: language })
//    }, [language])

//    return (
//       <AppContext.Provider value={{ store, actions }}>
//          {props.children}
//       </AppContext.Provider>
//    );
// }

// export { AppContext, AppProviderWrapper }

const cache = createIntlCache()
const Appcontext = createContext();

export default function AppProviderWrapper(props) {
    const [language, setlanguage] = useState(MessageLanguage.en_US)

    const setInit = (language) => {
        var intl = createIntl(language, cache);
        IntFormat.setIntl(intl);
    }

    const onChangeLanguage = (locale = 'vi-VN') => {
        let lang = {...language}
        if(locale === 'en-US') {
            lang = {...language,...MessageLanguage.en_US}
        }
        setInit(lang);
        setlanguage(lang);
    }
    const {children} = props;
    return (
        <GlobalStore language={language}
        changeLanguage={onChangeLanguage}>
            <IntlProvider {...language}>
                {children}
            </IntlProvider>     
        </GlobalStore>
    )
}

const GlobalStore = (props) => { 
    const {language,changeLanguage} =props;
    const [store, setstore] = useState({
        messLanguage: language,
        menus: []
    })

    const [actions, setactions] = useState({
        changeLanguage: changeLanguage,
        setMenu: (menus) => setstore({...store, menus: menus})
    })

    useEffect(() => {
        setstore({...store,messLanguage: language})
    }, [language])

    return (
        <Appcontext.Provider value={{store,actions}}>
            {props.children}
        </Appcontext.Provider>
    )
}
export {Appcontext, AppProviderWrapper}
