import 'semantic-ui-css/semantic.min.css'
import './App.css';
import Menus from './components/Navbar/Menu';
import {
  BrowserRouter,
  Switch,
  Route,
  Link
} from "react-router-dom";
import routes from './components/routes';
import HomePage from './modules/HomePage';
import Login from './modules/Login';
import ContactPage from './modules/ContactPage';
import Testformik from './modules/Testformik';

function App() {
  return (
    <BrowserRouter> 
      <div>
      <Menus/>
      <Switch>
          {routes.map((route,index) => {
            return (
              <Route path={route.path} exact={route.exact} component= {route.main}/>
            )
          })}
          {/* <Route path="/formik">
            <Testformik/>
           </Route>
          <Route path="/about"> 
            <Login /> 
          </Route>
          <Route path="/contact"> 
             <ContactPage /> 
           </Route>
          <Route path="/">
            <HomePage />
           </Route> */}
        </Switch>
        </div>
    </BrowserRouter>
  );
}
export default App;
