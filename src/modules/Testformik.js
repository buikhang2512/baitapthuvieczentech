import { Formik,FastField } from 'formik';
import React, { useState } from 'react';
import { Form } from 'semantic-ui-react';
import InputField from '../components/InputField';

export default function Testformik() {

    const [value, setvalue] = useState({});

    const options = [
        { key: 'm', text: 'Male', value: 'male' },
        { key: 'f', text: 'Female', value: 'female' },
        { key: 'o', text: 'Other', value: 'other' },
      ]
    const initialValues = {
        title: '',
    }
    
    const handleChange = (e, { value }) => setvalue({ value })  
    return (
        <div className="container">
            <Formik initialValues={initialValues}>
                {formikProps => {
                    // do something here

                    const {values,error,touched} = formikProps;
                    console.log({values,error,touched});  
                    return (
                        <Form>
                            <FastField
                                name="title"
                                component={InputField}

                                label="Title"
                                placeholder="Nhập cái gì đó"
                            />

                            <Form.Group widths='equal'>
                            <Form.Input fluid label='First name' placeholder='First name' />
                            <Form.Input fluid label='Last name' placeholder='Last name' />
                            <Form.Select
                                fluid
                                label='Gender'
                                options={options}
                                placeholder='Gender'
                            />
                            </Form.Group>
                            <Form.Group inline>
                            <label>Size</label>
                            <Form.Radio
                                label='Small'
                                value='sm'
                                checked={value === 'sm'}
                                onChange={handleChange}
                            />
                            <Form.Radio
                                label='Medium'
                                value='md'
                                checked={value === 'md'}
                                onChange={handleChange}
                            />
                            <Form.Radio
                                label='Large'
                                value='lg'
                                checked={value === 'lg'}
                                onChange={handleChange}
                            />
                            </Form.Group>
                            <Form.TextArea label='About' placeholder='Tell us more about you...' />
                            <Form.Checkbox label='I agree to the Terms and Conditions' />
                            <Form.Button>Submit</Form.Button>
                        </Form>
                    )
                }}
            </Formik>
        </div>
    )
}
