import React,{ useState, useEffect } from 'react'
import { useCallback } from 'react';
import { Button, Checkbox, Form } from 'semantic-ui-react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function Login() {
    const [listacout, setlistacout] = useState([{accout:'admin',pass:'admin'},{accout:'admin',pass:'12345'},{accout:'khang',pass:'2512'}])
    const [detail, setdetail] = useState({accout: "", pass: ""});

    const onAcoutchange = useCallback((e) => {
        console.log(e.target)  
    },[]);   
    
    const login = () => {
        var result = false;
        listacout.forEach(acout  => {
            if(acout.accout == detail.accout && acout.pass == detail.pass) {
                result = true;
            }
        });
        result ? toast.success("Đăng nhập thành công","Success") : toast.error("Sai tài khoản hoặc mật khẩu");
        
        return result;
    }

    return (
        <div style={{textAlign:"center"}}>
            <p>Đăng nhập</p>
            <div className="container">
                <ToastContainer/>
                <div className="form-login">
                    <Form onSubmit={login }>
                        <Form.Field>
                        <label>Tài khoản</label>
                        <input placeholder='nhập tài khoản' onChange={e => setdetail({...detail,accout: e.target.value})} />
                        </Form.Field>
                        <Form.Field>
                        <label>Mật khẩu</label>
                        <input type="password" placeholder='nhập mật khẩu' onChange={e =>setdetail({...detail,pass:e.target.value})} />
                        </Form.Field>
                        {/*  */}
                        <button className="ui button">Login</button>
                    </Form>
                </div>
            </div>
        </div>  
    )
}
