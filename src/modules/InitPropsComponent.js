import {
   ZenTableList, ZenFieldSelectApi, ZenDatePeriod, ZenLink,
   ZenLookupDropdown, HeaderLink, SegmentFooter, InputDate, ZenModalLookup, ZenLookupSearch, InputDateMY, ZenTableBody, ZenFormJsonSchema
} from "../components/Control";
import { ZenLookup } from "../ComponentInfo/Dictionary/ZenLookup";
import { getModal } from "../ComponentInfo/Dictionary/index";
import { ZenApp, auth, FormMode } from "../../utils";
import { ApiConfig, ApiLookup, ApiReport, ApiSettingGrid, ApiStored } from "../../Api";
import * as routes from "../../constants/routes";
import { GlobalStorage, KeyStorage } from "../../utils/storage";
import ZenTableListMobile from "../../components/Control/ZenTableListMobile";
import ZenFieldSelectApiMulti from "../../components/Control/ZenFieldSelectApiMulti";
import ZenEditor from "../../components/Control/ZenEditor";

// ko import trực tiếp các file vào trong Component
// khởi tạo Props default cố định cho component
// Được gọi trong Home khi đăng nhập
export function InitPropsComponent() {
   ZenTableList.defaultProps = {
      ...ZenTableList.defaultProps,
      getModal: getModal,
      baseUrl: ZenApp.baseUrl,
      hiddenSetting: false, // ẩn/hiện thiết lập table góc trái trên
      apiChangeCode: ApiConfig.changeCode,
      apiSettingGrid: {
         url: ApiSettingGrid,
         // default: get, getByCode, update
         // tên api khác default thì khai báo thêm, VD: get:"getAll", getByCode:"getSingle",... 
      },
   }
   ZenTableListMobile.defaultProps = {
      ...ZenTableListMobile.defaultProps,
      checkPermission: (permis) => auth.checkPermission(permis),
      colorItem: "blue"
   }

   ZenTableBody.defaultProps = {
      ...ZenTableBody.defaultProps,
      checkPermission: (permis) => auth.checkPermission(permis),
   }
   ZenFieldSelectApi.defaultProps = {
      ...ZenFieldSelectApi.defaultProps,
      keyStorageGlobal: KeyStorage.Global,
      api: { url: ApiLookup },
      getModal: getModal,
      checkPermission: (permis) => auth.checkPermission(permis),
      popupLookup: true,
   }
   ZenFieldSelectApiMulti.defaultProps = {
      ...ZenFieldSelectApiMulti.defaultProps,
      api: { url: ApiLookup },
      getModal: getModal,
      popupLookup: false,
   }
   ZenLookupSearch.defaultProps = {
      ...ZenLookupSearch.defaultProps,
      api: { url: ApiLookup },
      popupLookup: true
   }
   ZenLookupDropdown.defaultProps = {
      ...ZenLookupDropdown.defaultProps,
      api: { url: ApiLookup },
      popupLookup: true
   }
   ZenModalLookup.defaultProps = {
      ...ZenModalLookup.defaultProps,
      getModal: getModal,
      api: { url: ApiLookup },
      checkPermission: (permis) => auth.checkPermission(permis),
   }
   HeaderLink.defaultProps = {
      ...HeaderLink.defaultProps,
      routes: {
         home: routes.Home,
         settings: routes.Settings,
         reportCenter: routes.ReportModule,
      }
   }
   SegmentFooter.defaultProps = {
      color: "blue"
   }
   InputDate.defaultProps = {
      ...InputDate.defaultProps,
      financialYear: GlobalStorage.getByField(KeyStorage.Global, 'financial_year')
   }
   InputDateMY.defaultProps = {
      ...InputDateMY.defaultProps,
      financialYear: GlobalStorage.getByField(KeyStorage.Global, 'financial_year')
   }
   ZenDatePeriod.defaultProps = {
      ...ZenDatePeriod.defaultProps,
      keyLS: KeyStorage.Global   // key localstorage: dùng để set/get năm tài chính nếu có:{..., ngay_dntc, ngay_cntc}
   }
   ZenFormJsonSchema.defaultProps = {
      ...ZenFormJsonSchema.defaultProps,
      lookupInfo: ZenLookup
   }
   ZenLink.defaultProps = {
      ...ZenLink.defaultProps,
      checkPermission: (permis) => auth.checkPermission(permis),
   }
   ZenEditor.defaultProps = {
      ...ZenEditor.defaultProps,
      baseUrlApi: ZenApp.baseUrlAPI,
      route: "upload/editor", // route API
      token: auth.getToken(),
   }
}