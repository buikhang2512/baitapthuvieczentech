import React, { useState, useEffect,useCallback } from 'react'
import { Input, Table, Icon, Form,Select, Radio, Button } from 'semantic-ui-react';
import Todoitems from './../components/Todoitems';
import { GetTodo, AddTodo,DelTodo,UpdTodo } from './../action/action';
import { useSelector, useDispatch } from 'react-redux';
import axios from './../Api/axios';  
import * as config from './../constants/urlapi';
import { Mess_Component } from '../utils/zLanguage/variable';
import { IntFormat, LanguageFormat } from '../utils/intlFormat';
import { useIntl } from "react-intl";



export default function HomePage(props) {
    const [textInput, settextInput] = useState(" ");
    const [status, setstatus] = useState(false);
    const [isUpdate, setisUpdate] = useState(false);
    const [id, setid] = useState(' ');
    const todolist = useSelector((state) => state.todo); 
    const [isIntl, setisIntl] = useState(true)
    const {label,defaultlabel} = IntFormat.label(Mess_Component.CreateNew)
    const intl = useIntl();

    const activeOptions = [
        { key: 'm', text: 'Kích hoạt', value: 'true' },
        { key: 'f', text: 'Ẩn', value: 'false' },
      ]

    const dispatch = useDispatch();
    useEffect(() => {
        console.log(label,defaultlabel);
        async function fetchdata () {
            const request = await axios.get("zen");
            dispatch(GetTodo(request.data))
            return request;
        }
        fetchdata();
    }, [])


    const onTextinputchange = useCallback((e) => {
        settextInput(e.target.value);
    },[])
    const onStatusChange = (e) => {

        if(e.currentTarget.textContent == 'Kích hoạt') {
            setstatus('true');
        }
        if(e.currentTarget.textContent == 'Ẩn'){
            setstatus('false');
        }
        console.log(status);
    }

    const addTodo = () => {
        if(id == ' ' || id == null){
            axios.post(`zen`,{name:textInput,active:status},null).then(res => {
                console.log(res);
                if(res.status === 201) {
                    dispatch(AddTodo({name:textInput,active:status}))
                }
            })
        } else {
            console.log(id);
            axios.put(`zen/${id}`,{name:textInput,active:status},null).then(res => {
                console.log(res);
                if(res.status === 200) {
                    dispatch(UpdTodo({id:id,name:textInput,active:status}))
                    setisUpdate(false);
                    settextInput('');
                    setstatus('');
                }
            })
        }
    }
    const DeleteTodo = (id) => {
        console.log("xóa " + id);
        async function deldata () {
            const request = await axios.delete(`zen/${id}`,null,null)
            console.log(request);
            if(request.status === 200) {
                dispatch(DelTodo(id));
            }
        }
        deldata();
    }

    const UpdateTodo = (todo) => {
        settextInput(todo.name);
        setstatus(String(todo.active));
        setisUpdate(true);
        setid(todo.id);
    }
    // const showTableItems = (Todolist) => {
    //     var result = null;
    //     if(Todolist.length > 0) {
    //         result = Todolist.map((todo,index) => {
    //             return( <Todoitems todo = {todo}  />);
    //         })
    //     }
    //     return result;
    // }
    return (
        <div className="container">
            <div style={{ textAlign: "center" }}>
                <p>Danh sách việc cần làm</p>
            </div>
            <div className="form-todo">
                <Form>
                    <Form.Group widths='equal'>
                    <Form.Field
                        control={Input}
                        placeholder='Thêm công việc câm làm'
                        onChange={onTextinputchange}
                        value={textInput} 
                        // placeholder={isIntl ? intl.formatMessage({
                        //     id: (placeholder || label),
                        //     defaultMessage: (defaultplaceholder || defaultlabel || placeholder || label)
                        //  }) : (placeholder || label)}
                         label={isIntl ? intl.formatMessage({
                            id: (label),
                            defaultMessage: (defaultlabel || label)
                         }) : label
                         }
                    />
                    <Form.Field
                        control={Select}
                        value={status}
                        label='Trạng thái'
                        options={activeOptions}
                        placeholder='trạng thái'
                        onChange={onStatusChange}
                    />
                    </Form.Group>
                    <Button color='green'  onClick={addTodo}>{isUpdate ? 'Lưu' : 'Thêm'}</Button>
                </Form>
            </div>

            <Table celled>
                <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>STT</Table.HeaderCell>
                    <Table.HeaderCell>Công việc</Table.HeaderCell>
                    <Table.HeaderCell>Trạng thái</Table.HeaderCell>
                    <Table.HeaderCell>Action</Table.HeaderCell>
                </Table.Row>
                </Table.Header>

                <Table.Body>
                    {/* {showTableItems(todolist)} */}
                    <Todoitems todos = {todolist} DelTodo = {DeleteTodo} UpdateTodo= {UpdateTodo}/>
                {/* <Table.Row>
                    <Table.Cell>1</Table.Cell>
                    <Table.Cell>Unknown</Table.Cell>
                    <Table.Cell negative>None</Table.Cell>
                </Table.Row> */}
                </Table.Body>
            </Table>
        </div >
    )
}
