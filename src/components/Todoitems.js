import React, { useState } from 'react';
import { Input,Table,Icon,Button } from 'semantic-ui-react';

export default function Todoitems({todos, DelTodo, UpdateTodo}) {
    const [name,setname] = useState();
    const onInputchange = (e) => {
        setname(e.target.value);
    }
    return (
        <>
            {todos.map((todo,index) => {
                return (
                    <Table.Row>
                        <Table.Cell>{index+1}</Table.Cell>
                        <Table.Cell>{todo.name}</Table.Cell>
                        <Table.Cell>{todo.active ? "kích hoạt" : "Ẩn"}</Table.Cell>
                        <Table.Cell>
                            <Button color='violet' onClick = {() => UpdateTodo(todo)} >Sửa</Button>
                            <Button color='red' onClick={() => DelTodo(todo.id)} >Xóa</Button>
                        </Table.Cell>
                    </Table.Row>
                )
            })}   
        </>
    )
}
