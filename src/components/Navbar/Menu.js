import React, { useEffect, useState }  from 'react';
import {Link} from 'react-router-dom';
import {
  Checkbox,
  Grid,
  Header,
  Icon,
  Image,
  Menu,
  Segment,
  Sidebar,
} from 'semantic-ui-react';

export default function Menus() {
    const [visible, setVisible] = useState("false");
    const showmenu = () => {
        setVisible(!visible)
    }

    return (
        <div className="container">
            <div className="menuleft">
                <Sidebar
                as={Menu}
                animation='overlay'
                direction='left'
                inverted
                vertical
                visible={visible}
                >
                <Menu.Item header ><Link to="/">Note công việc</Link>
                
                </Menu.Item>
                <Menu.Item ><Link to="/contact">contact</Link></Menu.Item>
                <Menu.Item ><Link to="/about">đăng nhập</Link></Menu.Item>
                <Menu.Item ><Link to="/formik">Formik</Link></Menu.Item>
                <Menu.Item ><Link to="/">File Permissions</Link></Menu.Item>
                </Sidebar>
                <div className={!visible ? "btn-showmenu" : "btn-showmenu active"}  onClick = {showmenu}><Icon className="arr" name={!visible ? "arrow right" : "arrow left"}/></div>
            </div>
        </div>
    )
}
