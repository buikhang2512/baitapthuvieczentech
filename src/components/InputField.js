import React from 'react';
import PropTypes from 'prop-types';
import { Form } from 'semantic-ui-react';

InputField.propTypes = {
    field: PropTypes.object.isRequired,
    form: PropTypes.object.isRequired,

    type: PropTypes.string,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    disable: PropTypes.bool,
}

InputField.defaultProps = {
    type: 'text',
    label: '',
    placeholder: '',
    disable: false,
}

export default function InputField(props) {
    const {field,form,
        type,label,placeholder,disable} = props;

    // const {name,value,onChange,onBlue} = field;
    const {name} = field;
    return (
        <div>
             <Form.Group widths='equal'>
                <div className="field">
                    {label && <label for={name}>{label}</label>}
                    <div className="ui fluid input">
                        <input 
                             id={name}
                             // name = {name}
                             // value = {value}
                             // onChange = {onChange}
                             // onBlue = {onBlue}
                             {...field}
         
                             type={type}
                             disable={disable} 
                        type="text" placeholder="First name"/>
                    </div>
                </div>
                {/* <Form.Input fluid label='First name' placeholder='First name'
                    id={name}
                    // name = {name}
                    // value = {value}
                    // onChange = {onChange}
                    // onBlue = {onBlue}
                    {...field}

                    type={type}
                    disable={disable}
                    placeholder={placeholder}
                 /> */}
            </Form.Group>
        </div>
    )
}
