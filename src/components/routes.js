import Login from "../modules/Login";
import ContactPage from "../modules/ContactPage";
import HomePage from "../modules/HomePage";
import Testformik from "../modules/Testformik";

const routes = [
{
    path:'/formik',
    exact:false,
    main: () => <Testformik/>
},
{
    path:'/contact',
    exact: true,
    main: () => < ContactPage/>
},
{
    path:'/about',
    exact: true,
    main: () => < Login/>
},
{
    path:'/',
    exact: true,
    main: () => < HomePage />
}]

export default routes;