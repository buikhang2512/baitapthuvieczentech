import axios from 'axios';
import * as config from './../constants/urlapi';

// export const GetTodoFetchRequest = () => {
//     return dispatch => {
//         return axios.get(`${config.API_URL}/zen`,null,null).then(res => {
//             if(res.status === 200) {
//                 dispatch(GetTodo(res.data))
//             }
//         })
//     }
// }

export const GetTodo = (todo) => {
    return {
        type: 'GET_TODO',
        payload: todo,
    }
}

// export const AddTodoFetchRequest = (data) => {
//     console.log(data)
//     return dispatch => {
//         return axios.post(`${config.API_URL}/zen`,data,null).then(res => {
//             if(res.status === 201) {
//                 dispatch(GetTodo(data))
//             }
//         })
//     }
// }

export const AddTodo = (todo) => {
    return {
        type: 'ADD_TODO',
        payload: todo,
    }
}

export const DelTodo = (id) => {
    return {
        type: 'DEL_TODO',
        payload: id
    }
}
export const UpdTodo = (todo) => {
    return {
        type: 'UP_TODO',
        payload: todo
    }
}
