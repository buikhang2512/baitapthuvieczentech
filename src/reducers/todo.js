
const initialState = [];

var findID = (id,states) => {
    let result = -1;
    states.forEach((state,index) => {
      if(state.id === id) {
        result = index;
      }
    });
    return result;
  }

const todoReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_TODO': {
            state = action.payload;
            return[...state]
        }
        case 'ADD_TODO': {
            const newtodo = state;
            const id = +state[state.length -1].id +1;
            const newitem = {id,...action.payload };
            newtodo.push(newitem);
            return [...state]
        }
        case 'DEL_TODO': {
            const index = findID(action.payload,state);
            state.splice(index,1);
            return [...state]
        }
        case 'UP_TODO': {
          var {id,name,active} = action.payload;
          var index = findID(id,state);
          state[index] = {
              id,
              name,
              active : active === 'true' ? true : false
          }
          console.log(state);
          return [...state];
        }
        default:
            return state;
    }
}

export default todoReducer